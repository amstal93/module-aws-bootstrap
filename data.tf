#####
# Caller
#####

data "aws_caller_identity" "this" {}

#####
# IAM Policies & Roles
#####

data "aws_iam_policy_document" "bucket_policy" {
  statement {
    effect  = "Deny"
    actions = ["s3:*"]
    resources = [
      aws_s3_bucket.terraform_bucket.arn,
      "${aws_s3_bucket.terraform_bucket.arn}/*"
    ]
    principals {
      type        = "*"
      identifiers = ["*"]
    }
    condition {
      test     = "StringNotLike"
      variable = "aws:userId"
      values = distinct(concat(
        ["${aws_iam_role.terraform_bucket.unique_id}:*"],
        [for group in data.aws_iam_group.groups.*.users : group.*.user_id][0],
        [for key, role in aws_iam_role.terraform_bucket_teams : "${role.unique_id}:*"],
        var.allowed_aws_user_id_patterns,
        ["${split(":", data.aws_caller_identity.this.user_id)[0]}*"]
      ))
    }
  }
}

data "aws_iam_policy_document" "account_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.this.account_id}:root"
      ]
    }
  }
}

data "aws_iam_policy_document" "allow_all" {
  statement {
    sid = "1"

    actions = [
      "s3:*",
    ]

    resources = [
      aws_s3_bucket.terraform_bucket.arn,
    ]
  }

  statement {
    sid    = "AllowS3KMS"
    effect = "Allow"

    actions = [
      "kms:Get*",
      "kms:Describe*",
      "kms:Decrypt*",
      "kms:Encrypt",
      "kms:Sign",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey",
    ]

    resources = [aws_kms_key.terraform_bucket.arn]
  }
}

data "aws_iam_policy_document" "limited_access" {
  for_each = var.subdirectories_by_teams

  statement {
    sid    = "AllowListingBucket"
    effect = "Allow"

    actions = [
      "s3:ListBucket",
    ]

    resources = [aws_s3_bucket.terraform_bucket.arn]
  }

  statement {
    sid    = "AllowS3KMS"
    effect = "Allow"

    actions = [
      "kms:Get*",
      "kms:Describe*",
      "kms:Decrypt*",
      "kms:Encrypt",
      "kms:Sign",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey",
    ]

    resources = [aws_kms_key.terraform_bucket.arn]
  }

  statement {
    sid    = "AllowAllOnSubDirectory"
    effect = "Allow"

    actions = [
      "s3:Get*",
      "s3:List*",
      "s3:PutObject",
      "s3:PutObjectTagging",
      "s3:PutObjectVersionTagging",
      "s3:DeleteObject",
    ]

    resources = [
      format("%s/%s/*", aws_s3_bucket.terraform_bucket.arn, each.value),
    ]
  }
}

#####
# IAM Group
#####

data "aws_iam_group" "groups" {
  count = var.attach_subdirectories_policy_to_existing_groups ? length(keys(var.subdirectories_by_teams)) : 0

  group_name = keys(var.subdirectories_by_teams)[count.index]
}
