# Standard S3 repository for terraform

Configuration in this directory creates set of S3 resources to be used for terraform’s state files.

## Usage

To run this example you need to execute:

```bash
terraform init
terraform plan
terraform apply
```

Note that this example may create resources which can cost money (AWS Elastic IP, for example). Run `terraform destroy` when you don't need these resources.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |
| <a name="provider_random"></a> [random](#provider\_random) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_s3_bootstrap"></a> [s3\_bootstrap](#module\_s3\_bootstrap) | ../../ | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_iam_group.test](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group) | resource |
| [aws_iam_group_membership.member](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group_membership) | resource |
| [aws_iam_user.test](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user) | resource |
| [random_string.this](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_access_key"></a> [aws\_access\_key](#input\_aws\_access\_key) | n/a | `string` | `null` | no |
| <a name="input_aws_assume_role"></a> [aws\_assume\_role](#input\_aws\_assume\_role) | n/a | `string` | `null` | no |
| <a name="input_aws_profile"></a> [aws\_profile](#input\_aws\_profile) | n/a | `string` | `null` | no |
| <a name="input_aws_secret_key"></a> [aws\_secret\_key](#input\_aws\_secret\_key) | n/a | `string` | `null` | no |
| <a name="input_endpoint"></a> [endpoint](#input\_endpoint) | n/a | `string` | `null` | no |
| <a name="input_localstack_test"></a> [localstack\_test](#input\_localstack\_test) | n/a | `bool` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_iam_policy_arns"></a> [iam\_policy\_arns](#output\_iam\_policy\_arns) | n/a |
| <a name="output_iam_policy_ids"></a> [iam\_policy\_ids](#output\_iam\_policy\_ids) | n/a |
| <a name="output_iam_role_arns"></a> [iam\_role\_arns](#output\_iam\_role\_arns) | n/a |
| <a name="output_iam_role_ids"></a> [iam\_role\_ids](#output\_iam\_role\_ids) | n/a |
| <a name="output_iam_role_unique_ids"></a> [iam\_role\_unique\_ids](#output\_iam\_role\_unique\_ids) | n/a |
| <a name="output_kms_arn"></a> [kms\_arn](#output\_kms\_arn) | The key ARN for the S3 bucket for terraform state files. |
| <a name="output_kms_id"></a> [kms\_id](#output\_kms\_id) | The key id for the S3 bucket for terraform state files. |
| <a name="output_s3_arn"></a> [s3\_arn](#output\_s3\_arn) | The ARN of the S3 bucket for terraform state files. |
| <a name="output_s3_bucket_domain_name"></a> [s3\_bucket\_domain\_name](#output\_s3\_bucket\_domain\_name) | The domain name of S3 bucket for terraform state files. |
| <a name="output_s3_id"></a> [s3\_id](#output\_s3\_id) | The ID of the S3 bucket for terraform state files. |
| <a name="output_s3_region"></a> [s3\_region](#output\_s3\_region) | The region of S3 bucket for terraform state files. |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
