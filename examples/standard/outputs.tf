output "s3_id" {
  description = "The ID of the S3 bucket for terraform state files."
  value       = module.s3_bootstrap.s3_id
}

output "s3_arn" {
  description = "The ARN of the S3 bucket for terraform state files."
  value       = module.s3_bootstrap.s3_arn
}

output "s3_bucket_domain_name" {
  description = "The domain name of S3 bucket for terraform state files."
  value       = module.s3_bootstrap.s3_bucket_domain_name
}

output "s3_region" {
  description = "The region of S3 bucket for terraform state files."
  value       = module.s3_bootstrap.s3_region
}

output "kms_id" {
  description = "The key id for the S3 bucket for terraform state files."
  value       = module.s3_bootstrap.kms_id
}

output "kms_arn" {
  description = "The key ARN for the S3 bucket for terraform state files."
  value       = module.s3_bootstrap.kms_arn
}

output "iam_role_ids" {
  value = module.s3_bootstrap.iam_role_ids
}

output "iam_role_arns" {
  value = module.s3_bootstrap.iam_role_arns
}

output "iam_role_unique_ids" {
  value = module.s3_bootstrap.iam_role_unique_ids
}

output "iam_policy_ids" {
  value = module.s3_bootstrap.iam_policy_ids
}

output "iam_policy_arns" {
  value = module.s3_bootstrap.iam_policy_arns
}
