variable "aws_access_key" {
  default = null
  type    = string
}

variable "aws_secret_key" {
  default = null
  type    = string
}

variable "endpoint" {
  default = null
  type    = string
}

variable "localstack_test" {
  default = null
  type    = bool
}

variable "aws_profile" {
  default = null
  type    = string
}

variable "aws_assume_role" {
  default = null
  type    = string
}
